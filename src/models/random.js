
const getRandomIntegers = (start, end, length) => {
  const integers = [];
  for (let i = 0; i < length; i++) {
    let randomizedInt = start + Math.floor(Math.random() * end);
    while (integers.includes(randomizedInt)) {
      randomizedInt = start + Math.floor(Math.random() * end);
    }

    integers.push(randomizedInt);
  }

  return integers;
};

export {
  getRandomIntegers,
}