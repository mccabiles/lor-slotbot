export default {
  "Bilgewater": "Bilgewater",
  "Freljord": "Freljord",
  "BandleCity": "Bandle City",
  "Ionia": "Ionia",
  "PiltoverZaun": "Piltover & Zaun",
  "Noxus": "Noxus",
  "ShadowIsles": "Shadow Isles",
  "Demacia": "Demacia",
  "Shurima": "Shurima",
  "Targon": "Targon",
  "Runeterra": "Runeterra",
}